
import { composeWithDevTools } from "redux-devtools-extension"
import { createStore, applyMiddleware } from "redux"
import { rootReducer } from "../redux/rootReducer"
import { logger } from "redux-logger"

export const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(logger)))
store.dispatch({ type: "TEST_ACTION" })